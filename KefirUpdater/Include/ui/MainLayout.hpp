#pragma once
#include <pu/Plutonium>
#include <ui/MainApplication.hpp>
#include <net/Network.hpp>
#include <fs/FileSystem.hpp>
#include <Types.hpp>
#include <json.hpp>

class UpdateLayout;
class AboutLayout;
namespace ui
{
	class MainLayout : public pu::Layout
	{

	public:
		pu::element::Menu *menu;

		MainLayout();
		~MainLayout();
		void SetWaitState(bool stste, std::string const & text = std::string("Please wait"), int const & xPos = 500);

		void SetBarProgress(double current, double total);
		void SetBarText(std::string text);
		void SetBarVisibleState(bool state);

		void DebugLog(std::string text);

		virtual void OnShown();

	private:
		pu::element::Rectangle *waitRect;
		pu::element::TextBlock *waitDesc;
		
		pu::element::ProgressBar *bar;
		pu::element::TextBlock *barDesc;

		void OnUpdateClick();
		void OnAccountClick();
		void OnAboutClick();
		void OnExitClick();
		void OnRebootClick();
	};

	
}
