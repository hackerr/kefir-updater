#pragma once
#include <ui/MainLayout.hpp>

namespace ui
{
	class AboutLayout : public MainLayout
	{
	public:
		AboutLayout();
		~AboutLayout();

		void OnShown();

	private:
		pu::element::TextBlock *versionText;
		pu::element::TextBlock *changelogText;

		pu::element::TextBlock *updaterVersionText;
		pu::element::TextBlock *updaterChangelogText;

		bool isNewVersionChecked;

		void RetrieveData();
		void CheckForNewVersion();
	};


}