#pragma once
#include <ui/MainLayout.hpp>

namespace ui
{
	class AddAccountLayout : public MainLayout
	{
	public:
		AddAccountLayout();
		~AddAccountLayout();
		void OnShown();

	private:
		void RetrieveData();
	};


}