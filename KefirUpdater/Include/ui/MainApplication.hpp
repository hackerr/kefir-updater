#pragma once
#include <pu/Plutonium>
#include <net/Network.hpp>
#include <ui/MainLayout.hpp>
#include <curl/curl.h>
#include <string>


namespace ui
{

	class MainLayout;
	class AboutLayout;
	class UpdateLayout;
	class AddAccountLayout;
	
	// Define your application as a class too
	class MainApplication : public pu::Application
	{
	public:
		MainLayout *mainLayout;
		FsFileSystem systemFs;
		Service* accService;

		MainApplication();
		void Initialize();

		cfwu::ColorScheme DefaultScheme;
		bool isShowed;
		void LoadScheme();
		void CloseApp();
		void GoToUpdateLayout();
		void GoToAboutLayout();
		void GoToAccountLayout();

		std::string VERSION;

	private:

		AboutLayout *aboutLayout;
		UpdateLayout *updateLayout;
		AddAccountLayout* accountLayout;
	};

	static const cfwu::ColorScheme DefaultLight = { { 235, 235, 235, 255 }, { 220, 220, 220, 255 }, { 140, 140, 140, 255 }, { 15, 15, 15, 255 } };
	static const cfwu::ColorScheme DefaultDark = { { 45, 45, 45, 255 }, { 70, 70, 70, 255 }, { 110, 110, 110, 255 }, { 225, 225, 225, 255 } };
}