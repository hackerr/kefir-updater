#pragma once
#include <ui/MainLayout.hpp>

namespace ui
{
	class UpdateLayout : public MainLayout
	{
	public:
		UpdateLayout();
		~UpdateLayout();

		void OnShown();

	private:
		

		bool CheckLocalUpdates();
		void CheckRemoteUpdates();
	};


}