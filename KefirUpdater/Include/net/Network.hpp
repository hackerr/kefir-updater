#pragma once
#include <Types.hpp>
#include <curl/curl.h>

namespace net
{
	std::string RetrieveContent(std::string URL, std::string MIMEType = "");
	void RetrieveToFile(std::string URL, std::string Path, std::function<void(double Done, double Total)> Callback);
	bool CheckVersionDiff();
	bool HasConnection();
	bool Test();
}