#include <ui/AboutLayout.hpp>
#include <string>
#include <iostream>
#include <switch.h>
#include <iomanip> // setprecision
#include <sstream> // stringstream

namespace ui
{
	extern MainApplication *mainApp;

	AboutLayout::AboutLayout() : MainLayout::MainLayout()
	{
		this->isNewVersionChecked = false;

		this->versionText = new pu::element::TextBlock(420, 45, "", 25);
		this->changelogText = new pu::element::TextBlock(420, 100, "", 18);

		this->updaterVersionText = new pu::element::TextBlock(420, 300, "", 25);
		this->updaterChangelogText = new pu::element::TextBlock(420, 375, "", 18);

		this->versionText->SetColor(mainApp->DefaultScheme.Text);
		this->changelogText->SetColor(mainApp->DefaultScheme.Text);

		this->updaterVersionText->SetColor(mainApp->DefaultScheme.Text);
		this->updaterChangelogText->SetColor(mainApp->DefaultScheme.Text);

		this->Add(versionText);
		this->Add(changelogText);

		this->Add(updaterVersionText);
		this->Add(updaterChangelogText);
	}

	AboutLayout::~AboutLayout()
	{
	}

	void AboutLayout::OnShown() 
	{
		this->menu->SetSelectedIndex(3);

		RetrieveData();

		mainApp->CallForRender();

		if(!this->isNewVersionChecked)
			CheckForNewVersion();
	}

	void ui::AboutLayout::RetrieveData() 
	{
		if (!net::HasConnection())
		{
			std::string versionPath = "sdmc://switch/kefirupdater/version";
			if (fs::IsExist(versionPath))
			{
				std::string currentVersion = fs::ReadFile(versionPath);
				this->versionText->SetText("Local Kefir's version: " + currentVersion);
				this->versionText->SetY(25);
			}

			mainApp->CallForRender();

			mainApp->CreateShowDialog("Network error", "Check WiFi connection", { "Ok" }, true);
			return;
		}
		std::string version = net::RetrieveContent("https://switch.customfw.xyz/files/version", "application/json");
		std::string changelog = net::RetrieveContent("https://switch.customfw.xyz/files/changelog", "application/json");
		std::string newLine = "\n";

		std::string versionPath = "sdmc://switch/kefirupdater/version";
		if (fs::IsExist(versionPath))
		{
			std::string currentVersion = fs::ReadFile(versionPath);
			this->versionText->SetText("Remote Kefir's version: " + version+"\nLocal Kefir's version: "+ currentVersion);
			this->versionText->SetY(25);
		}
		else
		{
			this->versionText->SetText("Remote Kefir's version: " + version);
			this->versionText->SetY(45);

		}

		this->changelogText->SetText(changelog);

		mainApp->CallForRender();

		std::string updaterVersionJson = net::RetrieveContent("https://raw.githubusercontent.com/Povstalez/Kefir-Updater/master/version", "application/json");

		nlohmann::json json = nlohmann::json::parse(updaterVersionJson);
		std::string updaterVersion = json["version"].get<std::string>();
		std::string updaterChangelog = json["changelog"].get<std::string>();

		this->updaterVersionText->SetText("Remote Updater version: " + updaterVersion + "\nLocal Updater version: " + mainApp->VERSION);
		this->updaterChangelogText->SetText("Kefir Updater changelog:\n" + updaterChangelog);
	}


	void AboutLayout::CheckForNewVersion()
	{
		if (!net::HasConnection())
			return;

		this->isNewVersionChecked = true;
		std::string js = net::RetrieveContent("https://api.github.com/repos/povstalez/kefir-updater/releases/latest", "application/json");
		nlohmann::json j = nlohmann::json::parse(js);

		std::string remoteVersion = j["tag_name"].get<std::string>();

		cfwu::Version latestv = cfwu::Version::FromString(remoteVersion);
		cfwu::Version currentv = cfwu::Version::FromString(mainApp->VERSION);

		if (latestv.IsLower(currentv))
		{
			int answer = mainApp->CreateShowDialog("Kefir updater", "Finded new updater release. Update it?", { "Yes", "Cancel" }, true);
			if (answer == 0)
			{
				std::string latestUrl = j["assets"][0]["browser_download_url"].get<std::string>();
				std::string nroPath = "sdmc:/switch/kefirupdater/KefirUpdater.nro";

				if(!fs::IsExist("sdmc:/switch/kefirupdater"))
					nroPath = "sdmc:/switch/KefirUpdater.nro";

				fs::DeleteFile(nroPath);

				SetBarVisibleState(true);

				net::RetrieveToFile(latestUrl, nroPath, [&](double Done, double Total)
				{
					float totalF = Total / 1000000;
					float doneF = Done / 1000000;

					std::stringstream doness;
					doness << std::fixed << std::setprecision(1) << doneF;
					std::string doneStr = doness.str() + "mb";

					std::stringstream totalss;
					totalss << std::fixed << std::setprecision(1) << totalF;
					std::string totalStr = totalss.str() + "mb";


					float percentage = (Done / Total) * 100;

					std::stringstream percentagess;
					percentagess << std::fixed << std::setprecision(1) << percentage;
					std::string percentageStr = percentagess.str();

					this->SetBarProgress(Done, Total);
					this->SetBarText(percentageStr + "%\n" + doneStr + " / " + totalStr);

					mainApp->CallForRender();
				});

				SetBarVisibleState(false);

				int answer = mainApp->CreateShowDialog("Kefir updater", "Success. Please, reload app.\nReload now?", { "Yes", "No" }, true);
				if (answer == 0)
				{
					mainApp->CloseApp();
				}


			}
		}

	}

}