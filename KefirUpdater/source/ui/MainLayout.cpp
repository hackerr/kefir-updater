
#include <ui/MainLayout.hpp>
#include <string>
#include <iostream>
#include <switch.h>
#include <iomanip> // setprecision
#include <sstream> // stringstream

namespace ui
{
	extern MainApplication *mainApp;

	MainLayout::MainLayout()
	{
		pu::element::Rectangle *bgRect = new pu::element::Rectangle(0, 0, 1280, 720, mainApp->DefaultScheme.Base, 0);
		this->waitRect = new pu::element::Rectangle(0, 0, 1280, 720, pu::draw::Color(0,0,0,80), 0);
		this->waitDesc = new pu::element::TextBlock((u32)500, (u32)330, "Please wait", 40);
		this->waitDesc->SetColor(pu::draw::Color(255, 255, 255, 255));

		this->Add(bgRect);


		this->menu = new pu::element::Menu((u32)0, (u32)0, (u32)400, mainApp->DefaultScheme.Base, (u32)100, (u32)7);
		pu::element::Rectangle* delimer = new pu::element::Rectangle(400,0, 3, 720, mainApp->DefaultScheme.BaseFocus, 0);

		this->menu->SetOnFocusColor(mainApp->DefaultScheme.BaseFocus);
		this->menu->SetDrawShadow(false);


		pu::element::MenuItem *updateBtnItem = new pu::element::MenuItem("Check for update");
		pu::element::MenuItem *addAccountBtn = new pu::element::MenuItem("Add linked Account");
		pu::element::MenuItem *rebootBtnItem = new pu::element::MenuItem("Reboot to RCM");
		pu::element::MenuItem *aboutBtnItem = new pu::element::MenuItem("About");
		pu::element::MenuItem *exitBtnItem = new pu::element::MenuItem("Exit");

		updateBtnItem->SetColor(mainApp->DefaultScheme.Text);
		updateBtnItem->AddOnClick(std::bind(&ui::MainLayout::OnUpdateClick, this));
		addAccountBtn->SetColor(mainApp->DefaultScheme.Text);
		addAccountBtn->AddOnClick(std::bind(&ui::MainLayout::OnAccountClick, this));
		rebootBtnItem->SetColor(mainApp->DefaultScheme.Text);
		rebootBtnItem->AddOnClick(std::bind(&ui::MainLayout::OnRebootClick, this));
		aboutBtnItem->SetColor(mainApp->DefaultScheme.Text);
		aboutBtnItem->AddOnClick(std::bind(&ui::MainLayout::OnAboutClick, this));
		exitBtnItem->SetColor(mainApp->DefaultScheme.Text);
		exitBtnItem->AddOnClick(std::bind(&ui::MainLayout::OnExitClick, this));


		this->menu->AddItem(updateBtnItem);
		this->menu->AddItem(addAccountBtn);
		this->menu->AddItem(rebootBtnItem);
		this->menu->AddItem(aboutBtnItem);
		this->menu->AddItem(exitBtnItem);
		this->Add(this->menu);
		this->Add(delimer);



		pu::draw::Color fillColor = pu::draw::Color(13, 105, 180, 255);

		this->bar = new pu::element::ProgressBar((u32)590, (u32)600, (u32)510, (u32)20, 100);
		this->bar->SetProgressColor(fillColor);
		this->Add(bar);

		this->barDesc = new pu::element::TextBlock((u32)750, (u32)540, "");
		this->barDesc->SetColor(mainApp->DefaultScheme.Text);
		this->Add(barDesc);

		this->Add(this->waitRect);
		this->Add(this->waitDesc);

		SetWaitState(false);
		SetBarVisibleState(false);

	}

	MainLayout::~MainLayout()
	{
	}

	void MainLayout::OnUpdateClick()
	{
		mainApp->GoToUpdateLayout();
	}

	void MainLayout::SetWaitState(bool state, std::string const & text, int const & xPos)
	{
		this->waitRect->SetVisible(state);
		this->waitDesc->SetVisible(state);
		this->waitDesc->SetText(text);
		this->waitDesc->SetX(xPos);
	}

	void MainLayout::SetBarProgress(double current, double total)
	{
		this->bar->SetProgress(current);
		this->bar->SetMaxValue(total);
	}

	void MainLayout::SetBarText(std::string text)
	{
		this->barDesc->SetText(text);
	}

	void MainLayout::SetBarVisibleState(bool state)
	{
		this->bar->SetVisible(state);
		this->barDesc->SetVisible(state);
	}

	void MainLayout::OnShown()
	{
	}

	void MainLayout::DebugLog(std::string text)
	{
		pu::element::MenuItem *updateBtnItem = new pu::element::MenuItem(text);

		updateBtnItem->SetColor(mainApp->DefaultScheme.Text);
		updateBtnItem->AddOnClick(std::bind(&ui::MainLayout::OnUpdateClick, this));
		this->menu->AddItem(updateBtnItem);
	}

	void MainLayout::OnAboutClick()
	{
		mainApp->GoToAboutLayout();
	}

	void MainLayout::OnAccountClick()
	{
		mainApp->GoToAccountLayout();
	}

	void MainLayout::OnExitClick()
	{
		mainApp->CloseApp();
	}

	void MainLayout::OnRebootClick()
	{
		bpcRebootSystem();
		bpcExit();
	}
}
