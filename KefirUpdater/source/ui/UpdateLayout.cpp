#include <ui/UpdateLayout.hpp>
#include <string>
#include <iostream>
#include <switch.h>
#include <iomanip> // setprecision
#include <sstream> // stringstream
#include <chrono>
#include <thread>

namespace ui
{
	extern MainApplication *mainApp;

	UpdateLayout::UpdateLayout() : MainLayout::MainLayout()
	{
		
	}

	UpdateLayout::~UpdateLayout()
	{

	}

	void UpdateLayout::OnShown()
	{
		this->menu->SetSelectedIndex(0);

		mainApp->CallForRender();

		if (!CheckLocalUpdates())
			CheckRemoteUpdates();
	}

#pragma region Implement


	bool UpdateLayout::CheckLocalUpdates()
	{
		bool result = false;
		std::string updateFolderPath = "sdmc://Update/";

		fs::CreateDir(updateFolderPath);

		std::string updateFileName = "";

		if (fs::IsExist(updateFolderPath + "sxos.zip"))
			updateFileName = "sxos.zip";
		else if (fs::IsExist(updateFolderPath + "atmo.zip"))
			updateFileName = "atmo.zip";
		if (updateFileName != "")
		{
			std::string updatePath = updateFolderPath + updateFileName;
			int option = mainApp->CreateShowDialog("Finded local update", "Do you want update with " + updateFileName, { "Yes", "No", "Delete File" }, true);
			bool unzipRes = false;
			std::string unzipString = "";

			switch (option)
			{
			case 0:
				SetWaitState(true, "Try to unzip\nPlease wait, it may take up to several minutes", 230);
				mainApp->CallForRender();

				unzipRes = fs::Unzip(updatePath);

				unzipString = std::to_string(unzipRes);

				if (unzipRes)
				{
					SetWaitState(false);
					fs::DeleteFile(updatePath);
					mainApp->CallForRender();
					int rebootResult = mainApp->CreateShowDialog("Update finished", "Now you can reboot to RCM. Reboot now?", { "Yes", "No" }, true);

					if (rebootResult == 0)
					{
						bpcInitialize();
						bpcRebootSystem();
						bpcExit();
					}
				}
				else
					mainApp->CreateShowDialog("Kefir Updater", "Kefir update error. Now you can reboot to RCM", { "Ok" }, true);

				result = unzipRes;
				break;

			case 1:
				result = false;
				break;

			case -2:
				mainApp->CallForRender();
				fs::DeleteFile(updatePath);
				break;
			}
		}
		return result;
	}

	void UpdateLayout::CheckRemoteUpdates()
	{
		if (!net::HasConnection())
		{
			mainApp->CreateShowDialog("Network error", "Check WiFi connection", { "Ok" }, true);
			return;
		}

		std::string tempPath = "sdmc://Update";
		std::string kefirUpdaterRootFolder = "sdmc://switch/kefirupdater";
		fs::CreateDir(tempPath);
		fs::CreateDir(kefirUpdaterRootFolder);


		std::string curVersion = "0";

		/*int result = mainApp->CreateShowDialog("Updater", "Get last version number?", { "Yes!", "Cancel" }, true);
		if (result == 0)
		{*/
		std::string versionPath = "sdmc://switch/kefirupdater/version";
		if (fs::IsExist(versionPath))
			curVersion = fs::ReadFile(versionPath);

		SetWaitState(true);
		mainApp->CallForRender();

		std::string latestVersion = net::RetrieveContent("https://switch.customfw.xyz/files/version", "application/json");

		cfwu::Version latestv = cfwu::Version::FromString(latestVersion);
		cfwu::Version currentv = cfwu::Version::FromString(curVersion);

		SetWaitState(false);
		mainApp->CallForRender();

		if (latestv.IsEqual(currentv))
		{
			mainApp->CreateShowDialog("Kefir updater", "Kefir's version matches latest release's one.\nNo update required", { "Ok" }, true);
		}
		else if (latestv.IsLower(currentv))
		{
			int cfwOption = mainApp->CreateShowDialog("Kefir updater", "Finded new release " + latestVersion + "\nSelect your cfw", { "Atmosphere", "SX OS", "Cancel" }, true);

			int jsonIndex = -1;

			if (cfwOption == 0)
				jsonIndex = 0;
			else if (cfwOption == 1)
				jsonIndex = 3;
			else if (cfwOption == -1 || cfwOption == -2)
			{
				return;
			}

			SetBarVisibleState(true);

			std::string js = net::RetrieveContent("https://api.github.com/repos/rashevskyv/switch/releases/latest", "application/json");
			nlohmann::json j = nlohmann::json::parse(js);

			std::string updateUrl = j["assets"][jsonIndex]["browser_download_url"].get<std::string>();

			std::string updateFilePath = tempPath + "/archive.zip";
			if (fs::IsExist(updateFilePath))
				fs::DeleteFile(updateFilePath);

			net::RetrieveToFile(updateUrl, updateFilePath, [&](double Done, double Total)
			{
				float totalF = Total / 1000000;
				float doneF = Done / 1000000;

				std::stringstream doness;
				doness << std::fixed << std::setprecision(1) << doneF;
				std::string doneStr = doness.str() + "mb";

				std::stringstream totalss;
				totalss << std::fixed << std::setprecision(1) << totalF;
				std::string totalStr = totalss.str() + "mb";

				float percentage = (Done / Total) * 100;

				std::stringstream percentagess;
				percentagess << std::fixed << std::setprecision(1) << percentage;
				std::string percentageStr = percentagess.str();

				this->SetBarProgress(Done, Total);
				this->SetBarText(percentageStr + "%\n" + doneStr + " / " + totalStr);

				mainApp->CallForRender();
			});

			SetBarVisibleState(false);
			SetWaitState(true, "Try to unzip\nPlease wait, it may take up to several minutes", 230);
			mainApp->CallForRender();

			bool unzipRes = fs::Unzip(updateFilePath);

			SetWaitState(false);
			mainApp->CallForRender();
			if (unzipRes)
			{
				fs::WriteFile(versionPath, latestVersion);

				int rebootResult = mainApp->CreateShowDialog("Update finished", "Now you can reboot to RCM. Reboot now?", { "Yes", "No" }, true);

				if (rebootResult == 0)
				{
					bpcInitialize();
					bpcRebootSystem();
					bpcExit();
				}
			}
			else
				mainApp->CreateShowDialog("Kefir Updater", "Kefir update error", { "Ok" }, true);
		}
	}

#pragma endregion

}