#include <ui/MainApplication.hpp>
#include <ui/UpdateLayout.hpp>
#include <ui/AboutLayout.hpp>
#include <ui/AddAccountLayout.hpp>
#include <net/Network.hpp>
#include <stdlib.h>

namespace ui
{
	MainApplication *mainApp;

	MainApplication::MainApplication()
	{
		this->VERSION = "0.3.1";
		Initialize();

		this->isShowed = true;
		mainApp = this;
		LoadScheme();

		this->aboutLayout = new AboutLayout();
		this->updateLayout = new UpdateLayout();
		this->accountLayout = new AddAccountLayout();

		this->mainLayout = aboutLayout;
		this->LoadLayout(this->mainLayout);
		this->mainLayout->OnShown();
	}

	void MainApplication::Initialize()
	{
		pmshellInitialize();
		romfsInit();
		//accountInitialize();
		nifmInitialize();
		bpcInitialize();
	}

	void MainApplication::LoadScheme()
	{
		setsysInitialize();
		ColorSetId csid = ColorSetId_Light;
		setsysGetColorSetId(&csid);
		if (csid == ColorSetId_Dark) this->DefaultScheme = ui::DefaultDark;
		else this->DefaultScheme = ui::DefaultLight;
	}

	void MainApplication::CloseApp()
	{
		this->Close();
		this->isShowed = false;
	}

	void MainApplication::GoToUpdateLayout()
	{

		this->mainLayout = this->updateLayout;
		this->LoadLayout(this->mainLayout);
		this->mainLayout->OnShown();
	}

	void MainApplication::GoToAboutLayout()
	{
		this->mainLayout = this->aboutLayout;
		this->LoadLayout(this->mainLayout);
		this->mainLayout->OnShown();
	}
	void MainApplication::GoToAccountLayout()
	{
		this->mainLayout = this->accountLayout;
		this->LoadLayout(this->mainLayout);
		this->mainLayout->OnShown();
	}
}
