#if __INTELLISENSE__
typedef unsigned int __SIZE_TYPE__;
typedef unsigned long __PTRDIFF_TYPE__;
#define __attribute__(q)
#define __builtin_strcmp(a,b) 0
#define __builtin_strlen(a) 0
#define __builtin_memcpy(a,b) 0
#define __builtin_va_list void*
#define __builtin_va_start(a,b)
#define __extension__
#endif

#if defined(_MSC_VER)
#include <BaseTsd.h>
typedef SSIZE_T ssize_t;
#endif

#include <ui/MainApplication.hpp>
#include <switch.h>

int main(int argc, char* argv[])
{
	//consoleInit(NULL);
	// Create the application
	ui::MainApplication *amain = new ui::MainApplication();
	// Show it. This function will finalize when the application's "Close()" function is called.
	//socketInitializeDefault();
	//nxlinkStdio();
	
	amain->Show();
	while (amain->isShowed);
	/*{
		consoleUpdate(NULL);
	}
	socketExit();
	consoleExit(NULL);*/
	// IMPORTANT! free the application to destroy allocated memory and to finalize graphics.
	delete amain;
	// Exit
	return 0;
}