#include "ui/AddAccountLayout.hpp"
#include <string>
#include <iostream>
#include <filesystem>
#include <switch.h>
#include <iomanip> // setprecision
#include <sstream> // stringstream
#include <cstdio>
#include <cstring>

namespace ui
{
	extern MainApplication* mainApp;

	AddAccountLayout::AddAccountLayout()
	{
	}
	AddAccountLayout::~AddAccountLayout()
	{
	}

	void AddAccountLayout::OnShown()
	{
		this->menu->SetSelectedIndex(1);

		RetrieveData();
	}

	void AddAccountLayout::RetrieveData()
	{
		int warningResult = mainApp->CreateShowDialog("Warning", "All games saves will be lost.\nDump it first.", { "Inject", "Cancel" }, true);
		if (warningResult != 0)
		{
			mainApp->GoToAboutLayout();
			return;
		}

		warningResult = mainApp->CreateShowDialog("Warning", "Are you sure?", { "Yes", "Cancel" }, true);
		if (warningResult != 0)
		{
			mainApp->GoToAboutLayout();
			return;
		}

		if (!net::HasConnection())
		{
			mainApp->CreateShowDialog("Can't connent to server", "Please, connecto to the WiFi", { "Ok" }, true);
			return;
		}
		mainApp->CallForRender();

		if (!fs::DirExists("sdmc:/switch/kefirupdater/"))
			mkdir("sdmc:/switch/kefirupdater", 0777);

		if (!fs::IsExist("sdmc:/switch/kefirupdater/su.zip"))
			SetBarVisibleState(true);
		{
			net::RetrieveToFile("http://switch.customfw.xyz/files/su.zip", "sdmc:/switch/kefirupdater/su.zip", [&](double Done, double Total)
			{
				float totalF = Total / 1000000;
				float doneF = Done / 1000000;

				std::stringstream doness;
				doness << std::fixed << std::setprecision(1) << doneF;
				std::string doneStr = doness.str() + "mb";

				std::stringstream totalss;
				totalss << std::fixed << std::setprecision(1) << totalF;
				std::string totalStr = totalss.str() + "mb";


				float percentage = (Done / Total) * 100;

				std::stringstream percentagess;
				percentagess << std::fixed << std::setprecision(1) << percentage;
				std::string percentageStr = percentagess.str();

				if (percentageStr == "inf" || percentageStr == "nan")
					percentageStr = "0.0";

				this->SetBarProgress(Done, Total);
				this->SetBarText(percentageStr + "%\n" + doneStr + " / " + totalStr);

				mainApp->CallForRender();
			});

			mainApp->CallForRender();
		}

		bool unzipRes = fs::UnzipTo("sdmc:/switch/kefirupdater/su.zip", "sdmc:/switch/kefirupdater/");
		if (!unzipRes)
		{
			mainApp->CreateShowDialog("Error", "Failed unzip. Return.", { "Ok" }, true);
			return;
		}


		if (!R_SUCCEEDED(pmshellTerminateProcessByTitleId(0x010000000000001E)))
		{
			mainApp->CreateShowDialog("Abort", "Something went wrong. Please, try again later.", { "Ok" }, true);
			return;
		}
		pmshellTerminateProcessByTitleId(0x010000000000003E);
		pmshellTerminateProcessByTitleId(0x8000000000000010);

		mainApp->CallForRender();

		FsFileSystem mySystem;
		FsFileSystem acc;

		fsOpenBisFileSystem(&mySystem, 31, "");
		fsdevMountDevice("myssytem", mySystem);

		fsMount_SystemSaveData(&acc, 0x8000000000000010);
		fsdevMountDevice("account", acc);

		fs::copyDirToDir("sdmc:/switch/kefirupdater/su/", "account:/su/");

		fsdevCommitDevice("account");

		fsdevUnmountDevice("account");
		fsFsClose(&acc);

		mainApp->CallForRender();

		FsFileSystem saveSystem;
		FsFileSystem OLDsaveSystem;
		FsFile save;


		// this use for debuging
		/*if (!R_SUCCEEDED(fsMount_SystemSaveData(&saveSystem, 0x8000000000000010)))
		{
			fsFsDeleteFile(&mySystem, "/save/newAcc10");
			fsFsDeleteFile(&mySystem, "/save/newAcc11");
			fsFsRenameFile(&mySystem, "/save/8000000000000010", "/save/newAcc10");
			fsFsRenameFile(&mySystem, "/save/oldAcc10", "/save/8000000000000010");

			fsFsRenameFile(&mySystem, "/save/8000000000000011", "/save/newAcc11");
			fsFsRenameFile(&mySystem, "/save/oldAcc11", "/save/8000000000000011");

			fsFsDeleteFile(&mySystem, "/save/newAcc10");
			fsFsDeleteFile(&mySystem, "/save/newAcc11");
		}*/

		SetBarVisibleState(false);
		//SetWaitState(false);

		mainApp->CallForRender();

		int rebootResult = mainApp->CreateShowDialog("Inject accouint finished", "Now you need reboot to RCM. Reboot now?", { "Yes", "No" }, true);

		if (rebootResult == 0)
		{
			bpcInitialize();
			bpcRebootSystem();
			bpcExit();
		}
	}


}